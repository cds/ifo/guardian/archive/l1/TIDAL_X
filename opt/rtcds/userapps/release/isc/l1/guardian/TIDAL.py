from guardian import GuardState
import time
import cdsutils

#nominal = 'HPI_SERVO_ON_CTIDAL'


class INIT(GuardState):
    request = True

class SERVOS_OFF(GuardState):
    index = 30
    request = True

    def main(self):
        isi_ctrl_prefix = 'ISI-ETM' + ARM + '_ST1_SUSINF_' + ARM
        hpi_ctrl_prefix = 'HPI-ETM' + ARM + '_ISCMON_' + ARM

        control_hpi = cdsutils.avg(0.2, hpi_ctrl_prefix + '_OUTPUT') # find the amplitude of tidal (in nm)
        bleed_hpi = abs(control_hpi * 4 / 1000)   # fixed at 0.25 um/s (=4s/um)
        bleed_hpi_max = 900
        if bleed_hpi > bleed_hpi_max:
            bleed_hpi = bleed_hpi_max	# Ensure ramp time is reasonable

        ezca[isi_ctrl_prefix + '_TRAMP'] = 120
        ezca[hpi_ctrl_prefix + '_TRAMP'] = bleed_hpi # bleed_hpi # old = 300s
        time.sleep(0.3)
        ezca[isi_ctrl_prefix + '_GAIN'] = 0
        ezca[hpi_ctrl_prefix + '_GAIN'] = 0

    def run(self):
        hpi_ctrl_prefix = 'HPI-ETM' + ARM + '_ISCMON_' + ARM
        if not ezca[hpi_ctrl_prefix + '_OUTPUT'] == 0:
            notify('ramping down the gain')
            return False
        return True

##################################################################
# ISI

class PREP_ISI_SERVO_ON(GuardState):
    index = 200
    request = False

    def main(self):
        ctrl_prefix = 'ISI-ETM' + ARM + '_ST1_SUSINF_' + ARM
        ezca[ctrl_prefix + '_TRAMP'] = 0
        ezca[ctrl_prefix + '_RSET'] = 2
        time.sleep(0.3)

class ISI_SERVO_ON(GuardState):
    index = 220
    request = True

    def main(self):
        ctrl_prefix = 'ISI-ETM' + ARM + '_ST1_SUSINF_' + ARM
        ezca[ctrl_prefix + '_GAIN'] = ISIGAIN
        ezca.switch(ctrl_prefix, 'INPUT', 'ON')

    def run(self):
        if not ezca['IMC-IMC_TRIGGER_INMON'] >= 35:
            log('MC lost lock')
            return 'ISI_HOLD_OUTPUT'
        return True


class ISI_HOLD_OUTPUT(GuardState):
    index = 210
    request = True

    def main(self):
        ctrl_prefix = 'ISI-ETM' + ARM + '_ST1_SUSINF_' + ARM
        ezca.switch(ctrl_prefix, 'INPUT', 'OFF')

##################################################################
# HPI

class PREP_HPI_SERVO_ON(GuardState):
    index = 300
    request = False

    def main(self):
        ctrl_prefix = 'HPI-ETM' + ARM + '_ISCMON_' + ARM
        ezca[ctrl_prefix + '_TRAMP'] = 0
        ezca[ctrl_prefix + '_RSET'] = 2
        ezca[ctrl_prefix + '_LIMIT'] = 3.5e5
        ezca.switch(ctrl_prefix, 'LIMIT', 'ON')
        time.sleep(0.3)

#class HPI_SERVO_ON(GuardState):
#    index = 320
#    request = True
#
#    def main(self):
#        ctrl_prefix = 'HPI-ETM' + ARM + '_ISCMON_' + ARM
#        ezca[ctrl_prefix + '_GAIN'] = HPIGAIN
#        ezca.switch(ctrl_prefix, 'INPUT', 'ON')
#
#    def run(self):
#	if not ezca['IMC-IMC_TRIGGER_INMON'] >= 35:
#	    log('MC lost lock')
#	    return 'HPI_HOLD_OUTPUT'
#	return True

class HPI_SERVO_ON_CTIDAL(GuardState):
    index = 330
    request = True

    def main(self):
        
        # Send IMC-F #TODO: Move this to prep
        ezca['SUS-ETM'+ARM+'_TIDAL_MTRX_1_1'] = 0 #JCB, switching control from before TIDAL_M0 to after TIDAL_M0, 20191126
        ezca['SUS-ETM'+ARM+'_TIDAL_MTRX_1_2'] = 0
        ezca['SUS-ETM'+ARM+'_TIDAL_MTRX_1_3'] = 0
        ezca['SUS-ETM'+ARM+'_TIDAL_MTRX_1_4'] = 1
        ctrl_prefix = 'HPI-ETM' + ARM + '_ISCMON_' + ARM
        ezca.switch(ctrl_prefix,'FM3','ON','FM1','FM2','FM6','FM7','OFF')
        time.sleep(2)
        # Old Part
        ezca[ctrl_prefix + '_GAIN'] = HPIGAIN_CTIDAL
        ezca.switch(ctrl_prefix, 'INPUT', 'ON')

    def run(self):
        if not ezca['IMC-IMC_TRIGGER_INMON'] >= 35:
            log('MC lost lock')
            return 'HPI_HOLD_OUTPUT'
        return True


class HPI_SERVO_ON_DTIDAL(GuardState):
    index = 340
    request = True

    def main(self):

        # Send IMC-F #TODO: Move this to prep
        ezca['SUS-ETM'+ARM+'_TIDAL_MTRX_1_1'] = 0
        ezca['SUS-ETM'+ARM+'_TIDAL_MTRX_1_2'] = 0
        ezca['SUS-ETM'+ARM+'_TIDAL_MTRX_1_3'] = 0
        ezca['SUS-ETM'+ARM+'_TIDAL_MTRX_1_4'] = 1
        ctrl_prefix = 'HPI-ETM' + ARM + '_ISCMON_' + ARM
        ezca.switch(ctrl_prefix,'FM1','FM2','FM3','FM6','OFF','FM7','ON')
        time.sleep(2)
        # Old Part
        ezca[ctrl_prefix + '_GAIN'] = HPIGAIN_DTIDAL
        ezca.switch(ctrl_prefix, 'INPUT', 'ON')

    def run(self):
        if not ezca['IMC-IMC_TRIGGER_INMON'] >= 35:
            log('MC lost lock')
            return 'HPI_HOLD_OUTPUT'
        return True


class HPI_HOLD_OUTPUT(GuardState):
    index = 310
    request = True

    def main(self):
        ctrl_prefix = 'HPI-ETM' + ARM + '_ISCMON_' + ARM
        ezca.switch(ctrl_prefix, 'INPUT', 'OFF')

##################################################################

edges = [
    ('INIT', 'SERVOS_OFF'),
    ('SERVOS_OFF', 'PREP_ISI_SERVO_ON'),
    ('PREP_ISI_SERVO_ON', 'ISI_SERVO_ON'),
    ('ISI_SERVO_ON', 'ISI_HOLD_OUTPUT'),
    ('ISI_HOLD_OUTPUT', 'ISI_SERVO_ON'),
    ('ISI_HOLD_OUTPUT', 'SERVOS_OFF'),
    ('SERVOS_OFF', 'PREP_HPI_SERVO_ON'),
    #('PREP_HPI_SERVO_ON', 'HPI_SERVO_ON'),
    ('PREP_HPI_SERVO_ON', 'HPI_SERVO_ON_CTIDAL'),
    ('PREP_HPI_SERVO_ON', 'HPI_SERVO_ON_DTIDAL'),
    #('HPI_SERVO_ON', 'HPI_HOLD_OUTPUT'),
    ('HPI_SERVO_ON_CTIDAL', 'HPI_HOLD_OUTPUT'),
    ('HPI_SERVO_ON_DTIDAL', 'HPI_HOLD_OUTPUT'),
    #('HPI_HOLD_OUTPUT', 'HPI_SERVO_ON'),
    ('HPI_HOLD_OUTPUT', 'HPI_SERVO_ON_CTIDAL'),
    ('HPI_HOLD_OUTPUT', 'HPI_SERVO_ON_DTIDAL'),
    ('HPI_HOLD_OUTPUT', 'SERVOS_OFF'),
]


