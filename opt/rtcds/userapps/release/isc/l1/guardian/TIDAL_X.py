import TIDAL
TIDAL.ARM = 'X'
TIDAL.ISIGAIN = -2
TIDAL.HPIGAIN = -2
TIDAL.HPIGAIN_CTIDAL = -1 #Changed from -2 by JCB, 20191126, to account for taking after M0_TIDAL
TIDAL.HPIGAIN_DTIDAL = 1 #Changed from 0.4 by JCB, 20191126, to account for taking after M0_LOCK
from TIDAL import *
nominal = 'HPI_SERVO_ON_DTIDAL'
